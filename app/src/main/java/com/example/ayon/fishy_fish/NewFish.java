package com.example.ayon.fishy_fish;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * Created by Ayon on 2/19/2016.
 */
public class NewFish {

    private int x,y,dx;
    private Bitmap image ;
    Random rand;

    public NewFish(Bitmap res ) {
        rand = new Random() ;
        this.image = res;
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        this.x =rand.nextInt((1024 - 0) + 1) + 0;
        this.y = rand.nextInt((576-0)+1)+0;
        this.dx= -5;
    }
    public void update(){
        x=x+dx;
        if(x<0){
            this.x =rand.nextInt((1024 - 0) + 1) + 0;
            this.y = rand.nextInt((576-0)+1)+0;
        }

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }
    public void draw (Canvas canvas){

      canvas.drawBitmap(image,x,y,null);
    }

}
