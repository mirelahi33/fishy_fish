package com.example.ayon.fishy_fish;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CircleStarter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new Circle(this, 50, 50, 25));
    }
}
