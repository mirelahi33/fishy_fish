package com.example.ayon.fishy_fish;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Circle extends SurfaceView implements SurfaceHolder.Callback {

    private float x;
    private float y;
    private int radius;
    private Paint paint;
    private MainThread thread;
    Bitmap fish;
    float xx;
    float yy;
   private NewFish fish1;
    private NewFish fish2;
    private NewFish fish3;

    private boolean movingRight = true;

    public Circle(Context context, float x, float y, int radius) {
        super(context);
        //fish= BitmapFactory.decodeResource(getResources(),R.drawable.fish);
        this.x = x;
        this.y = y;
        this.radius = radius;

        // Tell the SurfaceHolder ( -> getHolder() ) to receive SurfaceHolder
        // callbacks
        getHolder().addCallback(this);
        this.thread = new MainThread(getHolder(), this);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.RED);
    }

    // Specify whether to move right or left

    public boolean onTouchEvent(MotionEvent event){
        if(event.getX() < this.x+50 && event.getX() > this.x-50 && event.getY() < this.y+50 && event.getY() > this.y-50){
            this.x = event.getX();
            this.y = event.getY();
        }

        return true;
    }

    public void moveCircle() {

    }

    public void onDraw(Canvas canvas) {

        final float scaleFactorWidth = (float)getWidth()/(float)1024;
        final float scaleFactorHeight=(float)getHeight()/(float)576;
        if(canvas!=null){

            final int savedState = canvas.save();
            canvas.scale(scaleFactorWidth, scaleFactorHeight);
            canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.background), 0, 0, null);

            canvas.restoreToCount(savedState);

        }

        canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.fish), this.x, this.y, null);
        fish1.draw(canvas);
        fish2.draw(canvas);
        fish3.draw(canvas);


        //  xx=yy=0;
        // canvas.drawBitmap(fish,100,100,paint);

    }

    public void clearCircle(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {

    fish1 = new NewFish(BitmapFactory.decodeResource(getResources(),R.drawable.object));
        fish2 = new NewFish(BitmapFactory.decodeResource(getResources(),R.drawable.object));
        fish3 = new NewFish(BitmapFactory.decodeResource(getResources(),R.drawable.object));
        //fish1.update();


        thread.start();
    }



    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        // TODO Auto-generated method stub
    }

    public void update(){

        if(this.x>=fish1.getX()-2 && this.x<= fish1.getX()+2 && this.y >= fish1.getY()-2 && this.y<=fish1.getY()+25 )
        {
        fish1.setX(2000);
            fish1.setY(2000);
            fish1.setDx(0);
        }
        if(this.x>=fish2.getX()-2 && this.x<= fish2.getX()+2 && this.y >= fish2.getY()-2 && this.y<=fish2.getY()+25   )
        {
            fish2.setX(2000);
            fish2.setY(2000);
            fish2.setDx(0);
        }
        if(this.x>=fish3.getX()-2 && this.x<= fish3.getX()+2 && this.y >= fish3.getY()-2 && this.y<=fish3.getY()+25  )
        {
            fish3.setX(2000);
            fish3.setY(2000);
            fish3.setDx(0);
        }
      /*  if(this.x>=fish1.getX()-2 && this.x<= fish1.getX()+2  )
        {
            fish1.setX(2000);
            fish1.setY(2000);
            fish1.setDx(0);
        }*/

        fish1.update();
        fish2.update();
        fish3.update();


    }

}