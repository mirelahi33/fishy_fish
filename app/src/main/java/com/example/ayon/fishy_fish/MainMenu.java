package com.example.ayon.fishy_fish;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainMenu extends Activity {
    Button play,score,about,exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu2);

        play = (Button) findViewById(R.id.buttonPlay);
        score = (Button) findViewById(R.id.buttonScore);
        about = (Button) findViewById(R.id.buttonAbout);
        exit = (Button) findViewById(R.id.buttonQuit);


              play.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent x = new Intent(getApplicationContext(),CircleStarter.class);
                startActivity(x);
            }
        });
        score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

        }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent y = new Intent(getApplicationContext(),About.class);
                startActivity(y);

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            System.exit(0);
            }
        });
    }







/*
 public void  play(View v){
     setContentView(new Circle(this, 50, 50, 25));
 }

    public void instruction(View v)
    {
        setContentView(R.layout.instruction);
    }
    public void bad(View v){
        System.exit(1);
    }
public void highscore(View v){

    setContentView(R.layout.highscore);

}

*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
